#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Paralelogramo: public FormaGeometrica {
    protected:
    float lado;
  public:
  Paralelogramo();
  ~Paralelogramo();
  Paralelogramo(float base, float altura);

    void setLado(float lado);
    float getLado();

  float calcula_area (float base, float altura);
  float calcula_perimetro(float lado, float altura);
};


#endif