#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Pentagono: public FormaGeometrica {
    protected:
    float apotema;

  public:
  Pentagono();
  ~Pentagono();
  Pentagono(float base, float apotema);

  void setApotema(float apotema);
  float getApotema();
  
  float calcula_area (float base, float apotema);
  float calcula_perimetro(float apotema, float perimetro);
};


#endif