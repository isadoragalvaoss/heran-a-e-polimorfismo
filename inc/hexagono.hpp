#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Hexagono: public FormaGeometrica {
    protected:
    float lado;

  public:
  Hexagono();
  ~Hexagono();
  Hexagono(float lado);

  void setLado(float lado);
  float getLado();

  float calcula_area (float lado);
  float calcula_perimetro(float lado);
};


#endif