#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Circulo: public FormaGeometrica {
    private:
    float pi;
    float raio;

  public:
  Circulo();
  ~Circulo();
  Circulo(float pi, float raio);

    void setRaio(float raio);
    float getRaio();

    void setPi(float pi);
    float getPi();

  float calcula_area (float pi, float raio);
  float calcula_perimetro(float pi, float raio);
};


#endif