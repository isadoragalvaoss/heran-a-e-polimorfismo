#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Triangulo: public FormaGeometrica {
  public:
  Triangulo();
  ~Triangulo();
  Triangulo(float base, float altura);

  float calcula_area();
  float calcula_perimetro(float base, float altura);
};


#endif