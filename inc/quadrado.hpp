#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Quadrado: public FormaGeometrica {

  public:
  Quadrado();
  ~Quadrado();
  Quadrado(float base, float altura);

  float calcula_area (float base, float altura);
  float calcula_perimetro(float base, float altura);
};


#endif