#include "circulo.hpp"
#include <iostream>
#include <math.h>
using namespace std;

Circulo:: Circulo (float pi, float raio){
    set_tipo("circulo");
    setRaio(raio);
    setPi(3.14);

    cout<< "novo objeto do tipo: "<< get_tipo()<<endl;
}

Circulo::Circulo(){
    set_tipo("circulo");
    setRaio(9.0);
    setPi(3.14);

    cout << "novo objeto do tipo: "<< get_tipo()<< endl;
}

Circulo::~Circulo(){
    cout<< "objeto destruido"<<endl;
}

void Circulo:: setRaio(float raio){
    this->raio=raio;
}
float Circulo:: getRaio(){
    return raio;
}

void Circulo:: setPi(float pi){
    this->pi= pi;
}
float Circulo:: getPi(){
    return pi;
}

float Circulo:: calcula_area(float pi, float raio){
    float area;
    area=pi*pow(raio,2);
    cout<< "area do objeto: "<< area<<endl;
    return area;
}

float Circulo:: calcula_perimetro(float pi, float raio){
    float perimetro;
    perimetro=2*pi*raio;
    cout<<"perimetro do objeto: "<<perimetro<< endl;
    return perimetro;
}