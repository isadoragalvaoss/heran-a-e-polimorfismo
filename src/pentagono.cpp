#include "pentagono.hpp"
#include <iostream>
#include <math.h>
using namespace std;


Pentagono::Pentagono(){
    set_tipo("pentagono");
    set_base(9.0);
    setApotema(3.0);

    cout << "novo objeto do tipo: "<< get_tipo()<< endl;
}
Pentagono:: Pentagono (float base, float apotema){
    set_tipo("pentagono");
    set_base(base);
    setApotema(apotema);

    cout<< "novo objeto do tipo: "<< get_tipo()<<endl;
}
Pentagono::~Pentagono(){
    cout<< "objeto destruido"<<endl;
}

void Pentagono:: setApotema(float apotema){
    this->apotema=apotema;
}
float Pentagono:: getApotema(){
    return apotema;
}

float Pentagono:: calcula_perimetro(float base, float apotema){
    float perimetro;
    float area;

    perimetro= 5*base;
    cout<<"perimetro do objeto: "<<perimetro<< endl;
    
    area= calcula_area(apotema,perimetro);
    cout<<"area do objeto: "<<area<<endl;
    return perimetro;
}

float Pentagono:: calcula_area(float apotema, float perimetro){

    return (apotema*perimetro)/2;
}