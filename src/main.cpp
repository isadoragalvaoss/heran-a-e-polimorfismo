#include "formageometrica.hpp"
#include "quadrado.hpp"
#include "triangulo.hpp"
#include "hexagono.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include <iostream>
#include <string>

using namespace std;

int main(){
    string tipo;
    float altura,base,apotema,lado,raio,pi;
    int i;
    char retorno;

    menu:
    cout<< "selecione uma das formas geometricas"<<endl;
    cout<< "1.quadrado"<<endl;
    cout<< "2.triangulo retangulo"<<endl;
    cout<< "3.paralelogramo"<<endl;
    cout<< "4.pentagono"<<endl;
    cout<< "5.hexagono"<<endl;
    cout<< "6.circulo"<<endl;

    cin >> i;

    if (i==1){
        system("clear");

        cout<< "digite o valor da base: ";
        cin>>base;
        cout<<"digite o valor da altura: ";
        cin>>altura;

        if(base!=altura){
            cout<<"impossivel"<<endl;
        }
        else{
            Quadrado q (base , altura);
            q.calcula_area(base, altura);
            q.calcula_perimetro(base,altura);
        }
    }

    cout<< "deseja repetir o procedimento? 1.sim 2.nao"<<endl;
    cin>>retorno;

    if(retorno==1){
        system("clear");
        goto menu;
    }
    else{
        system ("exit");
    }

    if (i==2){
        system("clear");

        Triangulo t (base, altura);
        t.calcula_area();

    cout<< "deseja repetir o procedimento? 1.sim 2.nao"<<endl;
    cin>>retorno;

    if(retorno==1){
        system("clear");
        goto menu;
    }
    else{
        system ("exit");
    }
    
    }

    if(i==3){
        system ("clear");

        cout<< "digite o valor da base: ";
        cin>> base;
        cout<< "digite o valor da altura: ";
        cin>>altura;

        if(base<=0 || altura <-0){
            cout<< "impossivel";
        }
        else{
            Paralelogramo p(base,altura);
            p.calcula_area(base,altura);

            cout<< "digite o valor do lado: ";
            cin>> lado;
            p.calcula_perimetro(base,lado);
        }

    cout<< "deseja repetir o procedimento? 1.sim 2.nao"<<endl;
    cin>>retorno;

    if(retorno==1){
        system("clear");
        goto menu;
    }
    else{
        system ("exit");
    }     
    }

    if(i==4){
        system ("clear");

        cout<<"digite o valor da base: "<<endl;
        cin>>base;
        cout<<"digite o valor da apotema: "<<endl;
        cin>>apotema;

        if (base<=0 || apotema<=0){
            cout<<"impossivel";
        }
        else{
            Pentagono pt(base,apotema);
            pt.calcula_perimetro(base,apotema);
        }

    cout<< "deseja repetir o procedimento? 1.sim 2.nao"<<endl;
    cin>>retorno;

    if(retorno==1){
        system("clear");
        goto menu;
    }
    else{
        system ("exit");
    }
    }

    if (i==5){
        system ("clear");

        cout<<"digite o valor do lado: "<<endl;
        cin>>lado;

        if (lado<=0){
            cout<<"impossivel";
        }
        else{
            Hexagono h(lado);
            h.calcula_area(lado);
            h.calcula_perimetro(lado);
        }

    cout<< "deseja repetir o procedimento? 1.sim 2.nao"<<endl;
    cin>>retorno;

    if(retorno==1){
        system("clear");
        goto menu;
    }
    else{
        system ("exit");
    }
    }


    if (i==6){
        system("clear");

        cout<< "digite o valor do raio: "<<endl;
        cin>>raio;
        pi=3.14;

        if (raio<=0){
            cout<<"impossivel";
        }
        else{
            Circulo c(pi,raio);
            c.calcula_area(pi,raio);
            c.calcula_perimetro(pi,raio);
        }

    cout<< "deseja repetir o procedimento? 1.sim 2.nao"<<endl;
    cin>>retorno;

    if(retorno==1){
        system("clear");
        goto menu;
    }
    else{
        system ("exit");
    }
    }

    return 0;
}

