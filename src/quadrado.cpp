#include "quadrado.hpp"
#include <iostream>
#include <string>
using namespace std;

Quadrado:: Quadrado (float base,float altura){
  set_tipo("quadrado");
  set_base(base);
  set_altura(altura);

  cout<< "novo objeto do tipo: "<< get_tipo()<< endl;
}

Quadrado:: Quadrado(){
  set_tipo("Quadrado");
  set_base(5.0);
  set_altura(5.0);

  cout<< "novo objeto do tipo "<< get_tipo() <<endl;
}

Quadrado::~Quadrado(){
  cout<< "objeto destruido"<< endl;
}

float Quadrado:: calcula_area(float base, float altura){
  float area;
  area= base*altura;

  cout<< "area do objeto: "<< area<< endl;

  return area;
}

float Quadrado:: calcula_perimetro(float base, float altura){
  float perimetro;
  perimetro= 2*base + 2*altura;

  cout<< "perimetro do objeto: "<< perimetro<< endl;

  return perimetro;
}