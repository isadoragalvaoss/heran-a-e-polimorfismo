#include "paralelogramo.hpp"
#include <iostream>
#include <math.h>
using namespace std;


Paralelogramo::Paralelogramo(){
    set_tipo("paralelogramo");
    set_base(9.0);
    set_altura(3.0);
    setLado(8);

    cout << "novo objeto do tipo: "<< get_tipo()<< endl;
}
Paralelogramo:: Paralelogramo (float base, float altura){
    set_tipo("paralelogramo");
    set_base(base);
    set_altura(altura);
    setLado(lado);

    cout<< "novo objeto do tipo: "<< get_tipo()<<endl;
}
Paralelogramo::~Paralelogramo(){
    cout<< "objeto destruido"<<endl;
}

void Paralelogramo:: setLado(float lado){
    this->lado=lado;
}
float Paralelogramo:: getLado(){
    return lado;
}

float Paralelogramo:: calcula_area(float base, float altura){
    float area;
    area=base*altura;
    cout<< "area do objeto: "<< area<<endl;
    return area;
}

float Paralelogramo:: calcula_perimetro(float lado, float base){
    float perimetro;
    perimetro=(2*lado) + (2*base);
    cout<<"perimetro do objeto: "<<perimetro<< endl;
    return perimetro;
}