#include "triangulo.hpp"
#include <iostream>
#include <string>
#include <math.h>
using namespace std;

Triangulo:: Triangulo(){
  set_tipo("triangulo retangulo");
  set_base(3.0);
  set_altura(4.0);

  cout<<"novo objeto do tipo: "<<get_tipo()<<endl;
}

Triangulo:: Triangulo(float base, float altura){
  set_tipo("triangulo retangulo");
  set_base(base);
  set_altura(altura);

  cout << "novo objeto do tipo: "<< get_tipo()<<endl;
}
Triangulo:: ~Triangulo(){
  cout<< "objeto destruido"<< endl;
}

float Triangulo:: calcula_area(){
  float area;
  float perimetro;

  cout<< "digite a base: ";
  cin>> this->base;
  cout<<" digite a altura: ";
  cin>> this->altura;

  if (altura<0 || base < 0 ){
    cout << "impossivel "<< endl;
  }
  else
  {
    area= ((get_base()*get_altura()))/2;
    cout<< "area do objeto: "<<area<<endl;

    perimetro= calcula_perimetro(base, altura);
    cout<< "perimetro do objeto: "<<perimetro<<endl;
  }
  return area;

}

float Triangulo:: calcula_perimetro(float base, float altura){
  float perimetro;
  float hipotenusa;
  hipotenusa = sqrt(pow(base,2)+pow(altura,2));
  perimetro = hipotenusa + base + altura;

  return perimetro;
}