#include "hexagono.hpp"
#include <iostream>
#include <math.h>
using namespace std;


Hexagono::Hexagono(){
    set_tipo("hexagono");
    set_altura(3.0);

    cout << "novo objeto do tipo: "<< get_tipo()<< endl;
}
Hexagono:: Hexagono (float lado){
    set_tipo("hexagono");
    setLado(lado);

    cout<< "novo objeto do tipo: "<< get_tipo()<<endl;
}
Hexagono::~Hexagono(){
    cout<< "objeto destruido"<<endl;
}

void Hexagono:: setLado(float lado){
    this->lado=lado;
}
float Hexagono:: getLado(){
    return lado;
}

float Hexagono:: calcula_area(float lado){
    float area;
    area=(3*sqrt(3)*pow(lado,2))/2;
    cout<< "area do objeto: "<< area<<endl;
    return area;
}

float Hexagono:: calcula_perimetro(float lado){
    float perimetro;
    perimetro= 6*lado;
    cout<<"perimetro do objeto: "<<perimetro<< endl;
    return perimetro;
}